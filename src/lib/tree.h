#ifndef _TREE_H
#define _TREE_H 0

/* Tree represents a node with a reference to content and pointers to link
 * itself to other nodes on the top, the sides, and the botton.
 */
typedef struct tree {
  void *content;
  struct tree *parent;
  struct tree *firstChild;
  struct tree *lastChild;
  struct tree *next;
  struct tree *prev;
} Tree;

Tree *treeNew(void *content);
Tree *treeAppend(Tree *t, void *content);
Tree *treeAppendAsChild(Tree *t, void *content);
void treeMap(Tree *t, void (*mapFunc)(void*, void*), void *userdata);
void treeFree(Tree *t, void (*freeFunc)(void *));
void treeUnlink(Tree *t);
void treeRemove(Tree *t, void (*freeFunc)(void *));
void treeMoveAfter(Tree *source, Tree *target);
char *treeToString(Tree *t, const char *fieldSep, const char *recordSep, 
                  char **(*contentToString)(void *content));
Tree *treeFromString(const char *treeString, const char *fieldSep, 
                     const char *recordSep, 
                     void *(*readFunc)(const char **record));

#endif /* _TREE_H */
