#ifndef _TEXTCONTENT_H
#define _TEXTCONTENT_H 0

typedef struct {
  int id;
  int ref;
  int pos;
  char *title;
  char *content;
} TextContent;

TextContent *textContentNew(int id, int ref, int pos, char *title,
                             char *content);

void textContentFree(TextContent *tc);

char **textContentToString(void *content);

#endif /* _TEXTCONTENT_H */
