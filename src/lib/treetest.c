#include <stdlib.h>
#include <assert.h>
#include "tree.h"

void accumContent(void *content, void *userdata)
{
  *(int*) userdata += *(int*) content;
}

void test()
{
  Tree *aux;
  int val = 1;
  int chk = 0;
  Tree *root = treeNew((void *) &val);
  treeAppend(root, (void *) &val);
  treeAppend(root, (void *) &val);
  aux = treeAppend(root, (void *) &val);
  treeAppendAsChild(aux, (void *) &val);
  treeMap(root, accumContent, (void*) &chk);
  assert(chk == 5);
  treeFree(root, NULL);
}

int main()
{
  test();
  return 0;
}
