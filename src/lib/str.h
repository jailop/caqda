#ifndef _STR_H
#define _STR_H 0

typedef struct {
  char *data;
  int len;
  int cap;
} String;

String *string_new(char *s);
void string_append(String *str, char *s);
void string_replace(String *str, char *pattern, char *replacement);
void string_free(String *str);
void vchar_free(char **vc);

#endif /* _STR_H */
