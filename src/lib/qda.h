#ifndef _QDA_H
#define _QDA_H 0

#include "tree.h"

typedef struct {
  Tree *docs;
  Tree *labels;
  Tree *marks;
  int idocs;
  int ilabels;
  int imarks;
} qda_t;

qda_t *qda_new();
void qda_free(qda_t *qda);

#endif /* _QDA_H */
