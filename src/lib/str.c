#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "str.h"

#define BUFSIZE 128

String *string_new(char *s)
{
  String *ret = malloc(sizeof(String));
  for (
    ret->len = strlen(s), ret->cap = BUFSIZE; 
    ret->len > ret->cap; 
    ret->cap *= 2
  ); 
  ret->data = malloc(sizeof(char) * (ret->cap + 1));
  strcpy(ret->data, s);
  return ret;
}

void string_append(String *str, char *s)
{
  int len;
  int changed = 0;
  if (!str) return;
  if (!s) return;
  if (s[0] == '\0') return;
  len = strlen(str->data);
  for (str->len += strlen(s); str->len > str->cap; changed = 1, str->cap *= 2);
  if (changed) {
    char *aux = realloc(str->data, str->cap);
    str->data = aux;
  }
  strcpy(str->data + len, s); 
}

void string_replace(String *str, char *pattern, char *replacement)
{
  char *aux;
  char *pos;
  int len;
  String *ret;
  if (!str) return;
  if (!pattern) return;
  if (!replacement) return;
  ret = string_new("");
  aux = str->data;
  len = strlen(pattern);
  while ((pos = strstr(aux, pattern))) {
    pos[0] = '\0';
    string_append(ret, aux);
    string_append(ret, replacement);
    aux = pos + len;
  }
  /* Appeding the remaining content */
  string_append(ret, aux);
  aux = str->data;
  str->data = ret->data;
  ret->data = aux;
  string_free(ret);
}

void string_free(String *str)
{
  free(str->data);
  free(str);
}

void vchar_free(char **vc)
{
  char **pos = vc;
  if (!pos) {
    fprintf(stderr, "[Warning] vchar_free: argument is null\n");
    return;
  }
  for (pos = vc; *pos; pos++)
    free(*pos);
  free(vc);
}
