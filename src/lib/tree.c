#include <stdio.h>
#include <stdlib.h>
#include "tree.h"

Tree *treeNew(void *content) {
  Tree *node = malloc(sizeof(Tree));
  node->content = content;
  node->parent = NULL;
  node->prev = NULL;
  node->next = NULL;
  node->firstChild = NULL;
  node->lastChild = NULL;
  return node;
}

Tree *treeAppend(Tree *t, void *content)
{
  Tree *pos;
  Tree *node = treeNew(content);
  if (t) {
    node->parent = t->parent;
    for (pos = t; pos->next; pos = pos->next);
    node->prev = pos;
    pos->next = node;
    if (t->parent)
      t->parent->lastChild = node;
  }
  return node;
}

Tree *treeAppendAsChild(Tree *t, void *content) 
{
  Tree *pos;
  Tree *node = treeNew(content);
  if (t) {
    node->parent = t;
    if (!(t->lastChild)) {
      t->firstChild = node;
    }
    else {
      pos = t->lastChild;
      pos->next = node;
      node->prev = pos;
    }
    t->lastChild = node;
  }
  return node;
}

void treeFree(Tree *t, void (*freeFunc)(void *))
{
  Tree *aux;
  Tree *pos = t;
  while (pos) {
    if (pos->firstChild)
      treeFree(pos->firstChild, freeFunc);
    if (freeFunc)
      freeFunc(pos->content);
    aux = pos->next;
    free(pos);
    pos = aux;
  }
}

void treeMap(Tree *t, void (*mapFunc)(void*, void*), void *userdata)
{
  Tree *pos = t;
  while (pos) {
    if (pos->firstChild)
      treeMap(pos->firstChild, mapFunc, userdata);
    mapFunc(pos->content, userdata);
    pos = pos->next;
  }
}

void treeUnlink(Tree *t)
{
  if (!t) {
    fprintf(stderr, "[Warning] treeUnlink: node to unlink is null\n");
    return;
  }
  if (t->prev)
    t->prev->next = t->next;
  if (t->next)
    t->next->prev = t->prev;
  if (t->parent->firstChild == t)
    t->parent->firstChild = t->next;
  if (t->parent->lastChild == t)
    t->parent->lastChild = t->prev;
}

void treeRemove(Tree *t, void (*freeFunc)(void *))
{
  if (!t) {
    fprintf(stderr, "[Warning] treeRemove: node to remove is null\n");
    return;
  }
  treeUnlink(t);
  treeFree(t, freeFunc);
}

void treeMoveAfter(Tree *source, Tree *target)
{
  if (!source) {
    fprintf(stderr, "[Warning] treeMoveAfter: source node is null\n");
    return;
  }
  if (!target) {
    fprintf(stderr, "[Warning] treeMoveAfter: target node is null\n");
    return;
  }
  treeUnlink(source);
  if (target->next)
    source->next = target->next;
  target->next = source;
  source->prev = target;
  source->parent = target->parent;
  if (target->parent->lastChild == target)
    target->parent->lastChild = source;
}

char *treeToString(Tree *t, const char *fieldSep, const char *recordSep, 
                  char **(*writeFunc)(void *content))
{
  Tree *pos = t;
  while (pos) {
    
  }
  return NULL;
}

Tree *treeFromString(const char *treeString, const char *fieldSep, 
                     const char *recordSep, 
                     void *(*readFunc)(const char **record))
{
  return NULL;
}
