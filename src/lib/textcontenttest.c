#include "str.h" 
#include "textcontent.h"

void test()
{
  TextContent *tc = textContentNew(0, 0, 0, "Text de prueba",
    "Pendiente de ser escrito");
  char **vc = textContentToString(tc);
  vchar_free(vc);
  textContentFree(tc);
}

int main()
{
  test();
  return 0;
}
