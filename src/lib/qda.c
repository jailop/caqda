#include <stdlib.h>
#include "qda.h"
#include "textcontent.h"

void node_free(void *node)
{
  TextContent *tc = (TextContent *) node;
  textContentFree(tc);
}

qda_t *qda_new()
{
  qda_t *qda = malloc(sizeof(qda_t));
  qda->docs = NULL;
  qda->labels = NULL;
  qda->marks = NULL;
  qda->idocs = -1;
  qda->ilabels = -1;
  qda->imarks = -1;
  return qda;
}

void qda_free(qda_t *qda)
{
  treeFree(qda->docs, node_free);
  treeFree(qda->labels, node_free);
  treeFree(qda->marks, node_free);
  free(qda);
}
