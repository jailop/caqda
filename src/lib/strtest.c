#include <string.h>
#include <assert.h>
#include "str.h"

void test()
{
  char *s1 = "hello";
  char *s2 = "world";
  String *str = string_new(s1);
  assert(strcmp(str->data, s1) == 0);
  string_append(str, s2);
  assert(strcmp(str->data, "helloworld") == 0);
  string_replace(str, "o", "ae");
  assert(strcmp(str->data, "hellaewaerld") == 0);
  string_replace(str, "x", "s");
  assert(strcmp(str->data, "hellaewaerld") == 0);
  string_free(str);
}

int main()
{
  test();
  return 0;
}
