#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "textcontent.h"

#define NFIELDS 5
#define BUFSIZE 24

TextContent *textContentNew(int id, int ref, int pos, char *title,
                             char *content)
{
  TextContent *tc = malloc(sizeof(TextContent));
  tc->id = id;
  tc->ref = ref;
  tc->pos = pos;
  tc->title = malloc(sizeof(char) * (strlen(title) + 1));
  tc->content = malloc(sizeof(char) * (strlen(content) + 1));
  strcpy(tc->title, title);
  strcpy(tc->content, content);
  return tc;
}

void textContentFree(TextContent *tc)
{
  free(tc->title);
  free(tc->content);
  free(tc);
}

char **textContentToString(void *content)
{
  int i;
  TextContent *tc;
  char **vecchar;
  if (!content) {
    fprintf(stderr, "[Warning] contentToString: content is NULL\n");
    return NULL;
  }
  tc = (TextContent *) content;
  vecchar = malloc(sizeof(char *) * (NFIELDS + 1));
  vecchar[NFIELDS] = NULL;
  for (i = 0; i < 5; i++) {
    if (i < 3)
      vecchar[i] = malloc(sizeof(char) * BUFSIZE);
    switch (i) {
      case 0:
        sprintf(vecchar[i], "%d", tc->id);
        break;
      case 1:
        sprintf(vecchar[i], "%d", tc->ref);
        break;
      case 2:
        sprintf(vecchar[i], "%d", tc->pos);
        break;
      case 3:
        vecchar[i] = malloc(sizeof(char) * (strlen(tc->title) + 1));
        strcpy(vecchar[i], tc->title);
        break;
      case 4:
        vecchar[i] = malloc(sizeof(char) * (strlen(tc->content) + 1));
        strcpy(vecchar[i], tc->content);
        break;
    }
  }
  return vecchar;
}
